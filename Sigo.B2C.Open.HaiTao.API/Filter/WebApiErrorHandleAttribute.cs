﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Sigo.Logger;
using Sigo.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http.Filters;

namespace Sigo.B2C.Open.HaiTao.API.Filter
{    
    /// <summary>
    /// 异常处理
    /// </summary>
    public class WebApiErrorHandleAttribute : ExceptionFilterAttribute
    { /// <summary>
      /// 重写基类的异常处理方法
      /// </summary>
      /// <param name="actionExecutedContext">上下文对象</param>
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            // 记录日志
            LogCore.AddError(
                System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name,
                IP.GetRealIP(),
                JsonConvert.SerializeObject(
                    new JObject(
                        new JProperty("project", "sigo.b2c.open.haitao"),
                        new JProperty("error", "请求Controller出现错误"),
                        new JProperty("exception", actionExecutedContext.Exception.Message))));

            var response = new HttpResponseMessage(HttpStatusCode.InternalServerError);
            response.Content = new StringContent(actionExecutedContext.Exception.Message, Encoding.UTF8, "application/json");
            actionExecutedContext.Response = response;
            base.OnException(actionExecutedContext);
        }
    }
}