﻿using Newtonsoft.Json;
using Sigo.B2C.Open.HaiTao.API.Filter;
using Sigo.B2C.Open.HaiTao.BLL.BusinessEntity;
using Sigo.B2C.Open.HaiTao.BLL.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Sigo.B2C.Open.HaiTao.API.Controllers
{
    /// <summary>
    /// 海淘单
    /// </summary>
    [WebApiErrorHandleAttribute]
    public class HaiTaoOrderController : ApiController
    {
        #region 基本属性
        private HaiTaoOrderService haiTaoOrderService = new HaiTaoOrderService();
        #endregion

        /// <summary>
        /// 电商暴露给海关的接口 必须Form格式接收
        /// </summary>
        /// <returns>返回结果</returns>
        [Route("platDataOpen")]
        [HttpPost]
        public HaiGuanResponseBEntity PlatDataOpen()
        {
            HaiGuanResponseBEntity response = new HaiGuanResponseBEntity();
            HaiGuanRequestBEntity requestEntity = JsonConvert.DeserializeObject<HaiGuanRequestBEntity>(HttpContext.Current.Request.Form[0]);
            try
            {
                response = haiTaoOrderService.PlatDataOpen(requestEntity);
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
            }

            return response;
        }
    }
}
