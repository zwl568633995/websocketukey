﻿using Sigo.B2C.Open.HaiTao.API.Filter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Sigo.B2C.Open.HaiTao.API
{
    /// <summary>
    /// config配置
    /// </summary>
    public static class WebApiConfig
    {
        /// <summary>
        /// 注册
        /// </summary>
        /// <param name="config">HttpConfiguration</param>
        public static void Register(HttpConfiguration config)
        {
            // Web API 配置和服务
            config.Formatters.Remove(config.Formatters.XmlFormatter); // 返回json

            // 跨域配置
            config.EnableCors(new EnableCorsAttribute("*", "*", "*"));

            // 异常过滤器
            config.Filters.Add(new WebApiErrorHandleAttribute());

            // Web API 路由
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional });
        }
    }
}
