﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sigo.B2C.Open.HaiTao.BLL.BusinessEntity
{
    /// <summary>
    /// 请求实体信息
    /// </summary>
    public class PayExInfo
    { 
        /// <summary>
        /// 请求数据
        /// </summary>
        public string PayExInfoStr { get; set; }
    }
}
