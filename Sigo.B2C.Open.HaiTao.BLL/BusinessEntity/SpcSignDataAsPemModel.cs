﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sigo.B2C.Open.HaiTao.BLL.BusinessEntity
{
    /// <summary>
    /// socket握手结果
    /// </summary>
    public class SpcSignDataAsPemModel
    {
        /// <summary>
        /// id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 方法
        /// </summary>
        public string Method { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// 结果
        /// </summary>
        public Args Args { get; set; }
    }
}
