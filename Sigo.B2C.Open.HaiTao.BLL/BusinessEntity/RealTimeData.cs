﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sigo.B2C.Open.HaiTao.BLL.BusinessEntity
{
    /// <summary>
    /// 实时数据实体
    /// </summary>
    public class RealTimeData
    { 
        /// <summary>
        /// 海关发起请求时，平台接收的会话ID
        /// </summary>
        public string SessionID { get; set; }

        /// <summary>
        /// 支付原始数据表头
        /// </summary>
        public Payexchangeinfohead PayExchangeInfoHead { get; set; }

        /// <summary>
        /// 支付原始数据表体
        /// </summary>
        public IEnumerable<Payexchangeinfolist> PayExchangeInfoLists { get; set; }

        /// <summary>
        /// 返回时的系统时间
        /// </summary>
        public string ServiceTime { get; set; }

        /// <summary>
        /// 证书编号
        /// </summary>
        public string CertNo { get; set; }

        /// <summary>
        /// 签名结果值
        /// </summary>
        public string SignValue { get; set; }
    }
}
