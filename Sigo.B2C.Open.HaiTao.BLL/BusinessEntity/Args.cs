﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sigo.B2C.Open.HaiTao.BLL.BusinessEntity
{
    /// <summary>
    /// socket返回的数据
    /// </summary>
    public class Args
    {
        /// <summary>
        /// 是否成功
        /// </summary>
        public bool Result { get; set; }

        /// <summary>
        /// 返回的数据
        /// </summary>
        public IEnumerable<string> Data { get; set; }

        /// <summary>
        /// 返回的错误结果
        /// </summary>
        public IEnumerable<string> Error { get; set; }
    }
}
