﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sigo.B2C.Open.HaiTao.BLL.BusinessEntity
{
    /// <summary>
    /// 商品实体
    /// </summary>
    public class Goodsinfo
    {
        /// <summary>
        /// 商品名称
        /// </summary>
        public string Gname { get; set; }

        /// <summary>
        /// 商品展示链接地址
        /// </summary>
        public string ItemLink { get; set; }
    }
}
