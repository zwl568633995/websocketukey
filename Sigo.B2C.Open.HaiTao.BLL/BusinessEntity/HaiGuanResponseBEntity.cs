﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sigo.B2C.Open.HaiTao.BLL.BusinessEntity
{
    /// <summary>
    /// 我们返回给海关的数据
    /// </summary>
    public class HaiGuanResponseBEntity
    {
        /// <summary>
        /// 返回code
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 返回消息
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// 发生时间timespan
        /// </summary>
        public string ServiceTime { get; set; }
    }
}
