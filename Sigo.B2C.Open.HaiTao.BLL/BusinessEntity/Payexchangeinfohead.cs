﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sigo.B2C.Open.HaiTao.BLL.BusinessEntity
{
    /// <summary>
    /// 实时数据头
    /// </summary>
    public class Payexchangeinfohead
    {
        /// <summary>
        /// 系统唯一序号
        /// </summary>
        public string Guid { get; set; }

        /// <summary>
        /// 原始请求
        /// </summary>
        public string InitalRequest { get; set; }

        /// <summary>
        /// 原始响应
        /// </summary>
        public string InitalResponse { get; set; }

        /// <summary>
        /// 电商平台代码
        /// </summary>
        public string EbpCode { get; set; }

        /// <summary>
        /// 支付企业代码
        /// </summary>
        public string PayCode { get; set; }

        /// <summary>
        /// 交易流水号
        /// </summary>
        public string PayTransactionId { get; set; }

        /// <summary>
        /// 交易金额
        /// </summary>
        public int TotalAmount { get; set; }

        /// <summary>
        /// 币制
        /// </summary>
        public string Currency { get; set; }

        /// <summary>
        /// 验核机构
        /// </summary>
        public string VerDept { get; set; }

        /// <summary>
        /// 支付类型
        /// </summary>
        public string PayType { get; set; }

        /// <summary>
        /// 交易成功时间
        /// </summary>
        public string TradingTime { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Note { get; set; }
    }
}
