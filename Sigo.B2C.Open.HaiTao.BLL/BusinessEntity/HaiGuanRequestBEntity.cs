﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sigo.B2C.Open.HaiTao.BLL.BusinessEntity
{
    /// <summary>
    /// 海关请求
    /// </summary>
    public class HaiGuanRequestBEntity
    {
        /// <summary>
        /// 单号
        /// </summary>
        public string OrderNo { get; set; }

        /// <summary>
        /// 请求ID
        /// </summary>
        public string SessionID { get; set; }

        /// <summary>
        /// 请求时间
        /// </summary>
        public long ServiceTime { get; set; }
    }
}
