﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sigo.B2C.Open.HaiTao.BLL.BusinessEntity
{
   /// <summary>
   /// 实体数据集合
   /// </summary>
    public class Payexchangeinfolist
    {
        /// <summary>
        /// 订单编号
        /// </summary>
        public string OrderNo { get; set; }

        /// <summary>
        /// 商品信息
        /// </summary>
        public Goodsinfo[] GoodsInfo { get; set; }

        /// <summary>
        /// 收款账号
        /// </summary>
        public string RecpAccount { get; set; }

        /// <summary>
        /// 收款企业代码
        /// </summary>
        public string RecpCode { get; set; }

        /// <summary>
        /// 收款企业名称
        /// </summary>
        public string RecpName { get; set; }
    }
}
