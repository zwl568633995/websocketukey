﻿using Newtonsoft.Json;
using Sigo.B2C.Open.HaiTao.BLL.BusinessEntity;
using Sigo.B2C.Open.HaiTao.BLL.DataEntity;
using Sigo.B2C.Open.HaiTao.BLL.Utils;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using WebSocketSharp;

namespace Sigo.B2C.Open.HaiTao.BLL.Service
{
    /// <summary>
    /// HaiTaoOrder加密传送
    /// </summary>
    public class HaiTaoOrderService
    {
        /// <summary>
        /// 电商暴露给海关的接口
        /// </summary>
        /// <param name="requestEntity">请求实体</param>
        /// <returns>返回结果1000成功，其他失败</returns>
        public HaiGuanResponseBEntity PlatDataOpen(HaiGuanRequestBEntity requestEntity)
        {
            string serviceTime = TimeSpanHelper.GetTimeSpan(DateTime.Now).ToString(); // 调用的时间
            string certNo = "01219957"; // Sigo证书编码
            string ebpCode = "3201962F40";  // Sigo企业代码
            string password = "88888888"; // U盾的密码

            ////1.返回结果，只返回调用接口的结果，调用即成功，其余传单操作交给开发处理
            HaiGuanResponseBEntity response = new HaiGuanResponseBEntity
            {
                Code = "10000",
                Message = "",
                ServiceTime = serviceTime
            };

            #region 日志记录海关的请求【持久化到数据库】
            using (SqlConnection conn = new BaseService().OpenSigoB2CConnection())
            {
                string sExcute = string.Format(
                    @"INSERT INTO HaiGuan_Content(OrignRequest,SigoResponse,Adder,AddTime,Moder,ModTime,SessionID,IsSuccess) 
                                                 VALUES('{0}','{1}',10,'{2}',10,'{2}','{3}',{4})", JsonConvert.SerializeObject(requestEntity),
                    JsonConvert.SerializeObject(response),
                    DateTime.Now.ToString(), 
                    requestEntity.SessionID, 
                    0);
                using (SqlCommand cmd = new SqlCommand(sExcute, conn))
                {
                    int rows = cmd.ExecuteNonQuery();
                }
            }
            #endregion

            WebSocketHelper webHelper = new WebSocketHelper();
            if (webHelper.WebSocket == null)
            {
                response.Message = "websocket未打开";
                return response;
            }

            for (int i = 0; i < 5; i++)
            {
                if (webHelper.WebSocket.ReadyState != WebSocketState.Open)
                {
                    webHelper.WebSocket.Connect();
                }
                else
                {
                    break;
                }
            }

            ////2.组装数据
            var sessionID = requestEntity.SessionID;
            string sousecData = "{\"sessionID\":\"" + sessionID + "\",\"payExchangeInfoHead\":{\"guid\":\"9D55BA71-55DE-41F4-8B50-C36C83B3B419\",\"initalRequest\":\"xxxxxxxxxxx\",\"initalResponse\":\"ok\",\"ebpCode\":\"" + ebpCode + "\",\"payCode\":\"312226T001\",\"payTransactionId\":\"2018726129\",\"totalAmount\":100,\"currency\":\"142\",\"verDept\":\"3\",\"payType\":\"1\",\"tradingTime\":\"20181212041803\",\"note\":\"批量订单，测试订单优化,生成多个so订单\"},\"payExchangeInfoLists\":[{\"orderNo\":\"SO1710301150602574003\",\"goodsInfo\":[{\"gname\":\"lhy-gnsku3\",\"itemLink\":\"http://m.yunjiweidian.com/yunjibuyer/static/vue-buyer/idc/index.html#/detail?itemId=999761&shopId=453\"}],\"recpAccount\":\"OSA571908863132601\",\"recpCode\":\"\",\"recpName\":\"YUNJIHONGKONGLIMITED\"}],\"serviceTime\":\"" + serviceTime + "\",\"certNo\":\"" + certNo + "\",\"signValue\":\"\"}";
            RealTimeData concurentRealTimeData = JsonConvert.DeserializeObject<RealTimeData>(sousecData);

            ////3.调用webSocket进行异步U盾加密数据
            var sb = new StringBuilder();
            sb.Append("\"sessionID\":");
            sb.Append($"\"{concurentRealTimeData.SessionID}\"");
            sb.Append($"||");
            sb.Append("\"payExchangeInfoHead\":");
            sb.Append($"\"{JsonConvert.SerializeObject(concurentRealTimeData.PayExchangeInfoHead, Formatting.None, JsonHelper.JsonSerializerSettings)}\"");
            sb.Append($"||");
            sb.Append("\"payExchangeInfoLists\":");
            sb.Append($"\"{JsonConvert.SerializeObject(concurentRealTimeData.PayExchangeInfoLists, Formatting.None, JsonHelper.JsonSerializerSettings)}\"");
            sb.Append($"||");
            sb.Append($"\"serviceTime\":\"{serviceTime}\"");

            var inData = sb.ToString();
            inData = inData.Replace("\"", "\\\"");
            var msg = "{\"_method\":\"cus-sec_SpcSignDataAsPEM\",\"_id\":0,\"args\":{\"inData\":\"" + inData + "\",\"passwd\":\"" + password + "\"}}";
            webHelper.ConcurentRealTimeData = concurentRealTimeData;
            webHelper.WebSocket.Send(msg);

            return response;
        }
    }
}
