﻿using Sigo.B2C.Open.HaiTao.BLL.Utils;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sigo.B2C.Open.HaiTao.BLL.Service
{
    /// <summary>
    /// 基础请求
    /// </summary>
    public class BaseService
    {
        private static string connString = ConfigManager.SigocnConstr;

        private static string connStringBack = ConfigManager.SigoBackEndConstr;

        private static string connStringSigoB2C = ConfigManager.SigoB2cConstr;

        private static string connStringSigoCn = ConfigManager.Sigocn100Constr;

        /// <summary>
        /// 返回Sigocn数据库
        /// </summary>
        /// <returns>连接</returns>
        public SqlConnection OpenSigoConnection()
        {
            SqlConnection connection = new SqlConnection(connString);
            connection.Open();
            return connection;
        }

        /// <summary>
        /// 返回Back数据库
        /// </summary>
        /// <returns>连接</returns>
        public SqlConnection OpenBackConnection()
        {
            SqlConnection connection = new SqlConnection(connStringBack);
            connection.Open();
            return connection;
        }

        /// <summary>
        /// 返回SigoB2C数据库
        /// </summary>
        /// <returns>连接</returns>
        public SqlConnection OpenSigoB2CConnection()
        {
            SqlConnection connection = new SqlConnection(connStringSigoB2C);
            connection.Open();
            return connection;
        }

        /// <summary>
        /// 返回Sigocn数据库
        /// </summary>
        /// <returns>连接</returns>
        public SqlConnection OpenSigoCnConnection()
        {
            SqlConnection connection = new SqlConnection(connStringSigoCn);
            connection.Open();
            return connection;
        }
    }
}
