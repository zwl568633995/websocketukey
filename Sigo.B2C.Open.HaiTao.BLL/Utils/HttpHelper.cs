﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Sigo.B2C.Open.HaiTao.BLL.Utils
{
    /// <summary>
    /// Http请求
    /// </summary>
    public class HttpHelper
    {
        /// <summary>
        /// 表单请求方式
        /// </summary>
        /// <typeparam name="T">泛型实体</typeparam>
        /// <param name="entity">实体</param>
        /// <param name="propertyName">属性名称</param>
        /// <param name="urlPathEncode">是否encode</param>
        /// <param name="ignoreProList">默认null</param>
        /// <param name="ignoreNullValue">默认true</param>
        /// <param name="sortName">默认false</param>
        /// <returns>请求结果</returns>
        public static string BuildFormPostData<T>(T entity, string propertyName, bool urlPathEncode = false, IEnumerable<string> ignoreProList = null, bool ignoreNullValue = false, bool sortName = false)
            where T : class
        {
            var type = entity.GetType();
            var prop = type.GetProperties(BindingFlags.Instance | BindingFlags.Public) as IEnumerable<PropertyInfo>;
            if (ignoreProList != null && ignoreProList.Any())
            {
                prop = prop.Where(p => !ignoreProList.Contains(p.Name) && (p.GetValue(entity, null) != null));
            }

            if (ignoreNullValue)
            {
                prop = prop.Where(p => p.GetValue(entity, null) != null);
            }

            if (sortName)
            {
                prop = prop.OrderBy(o => o.Name, StringComparer.Ordinal);
            }

            string postData = string.Empty;
            if (urlPathEncode)
            {
                postData = string.Join("&", prop.Select(p => $"{propertyName}={System.Web.HttpUtility.UrlEncode(p.GetValue(entity, null).ToString(), Encoding.UTF8)}"));
            }
            else
            {
                postData = string.Join("&", prop.Select(p => $"{propertyName}={p.GetValue(entity, null).ToString()}"));
            }

            return postData;
        }

        /// <summary>
        /// Post请求
        /// </summary>
        /// <param name="url">url</param>
        /// <param name="postData">数据</param>
        /// <param name="contentType">数据类型</param>
        /// <returns>请求结果</returns>
        public static Tuple<bool, string> Post(string url, string postData, string contentType)
        {
            var post = ApiHttpRequest(url, postData: postData, encoding: Encoding.UTF8, contentType: contentType);
            return post;
        }

        /// <summary>
        /// POST请求操作
        /// </summary>
        /// <param name="url">url</param>
        /// <param name="headerDic">null</param>
        /// <param name="method">方法</param>
        /// <param name="postData">数据</param>
        /// <param name="contentType">contentType</param>
        /// <param name="encoding">encoding</param>
        /// <returns>请求结果</returns>
        protected static Tuple<bool, string> ApiHttpRequest(string url, IDictionary<string, string> headerDic = null, string method = "POST", string postData = null, string contentType = "application/json", Encoding encoding = null)
        {
            string json = string.Empty;
            bool success = true;
            try
            {
                var header = new WebHeaderCollection();
                header.Add("Content-Type", contentType);
                if (headerDic != null)
                {
                    foreach (var item in headerDic)
                    {
                        header.Add(item.Key, item.Value);
                    }
                }

                Encoding myEncoding = encoding ?? Encoding.UTF8;
                var webClient = new WebClient();
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                webClient.Headers = header;
                if (method.ToLower() == "post")
                {
                    var buffer = myEncoding.GetBytes(postData);
                    var result = webClient.UploadData(url, method, buffer);
                    json = myEncoding.GetString(result);
                }
                else
                {
                    webClient.Encoding = myEncoding;
                    json = webClient.DownloadString(url);
                }
            }
            catch (Exception ex)
            {
                success = false;
                json = ex.Message;
            }
            finally
            {
            }

            return new Tuple<bool, string>(item1: success, item2: json);
        }
    }
}
