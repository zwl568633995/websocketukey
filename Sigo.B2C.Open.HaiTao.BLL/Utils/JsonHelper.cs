﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sigo.B2C.Open.HaiTao.BLL.Utils
{
    /// <summary>
    /// json序列化帮助类
    /// </summary>
    public class JsonHelper
    {
        /// <summary>
        /// 驼峰序列化
        /// </summary>
        private static JsonSerializerSettings jsonSerializerSettings;

        /// <summary>
        /// 驼峰序列化
        /// </summary>
        public static JsonSerializerSettings JsonSerializerSettings
        {
            get
            {
                if (jsonSerializerSettings == null)
                {
                    jsonSerializerSettings = new JsonSerializerSettings
                    { 
                        // 设置为驼峰命名
                        ContractResolver = new CamelCasePropertyNamesContractResolver()
                    };
                }

                return jsonSerializerSettings;
            }
        }
    }
}
