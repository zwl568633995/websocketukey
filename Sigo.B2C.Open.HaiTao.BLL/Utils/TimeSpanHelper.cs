﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sigo.B2C.Open.HaiTao.BLL.Utils
{
    /// <summary>
    /// 时间
    /// </summary>
    public class TimeSpanHelper
    {
        /// <summary>
        /// DateTime To TimeSpan
        /// </summary>
        /// <param name="dt">时间</param>
        /// <returns>时间戳</returns>
        public static long GetTimeSpan(DateTime dt)
        {
            System.DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1)); // 当地时区
            long timeStamp = (long)(dt - startTime).TotalMilliseconds; // 相差毫秒数
            return timeStamp;
        }
    }
}
