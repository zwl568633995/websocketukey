﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sigo.B2C.Open.HaiTao.BLL.Utils
{
    /// <summary>
    /// 数据库连接配置
    /// </summary>
    public class ConfigManager
    {
        /// <summary>
        /// Sigocn库
        /// </summary>
        public static string SigocnConstr
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["SqlSigocnConnStr"].ConnectionString;
            }
        }

        /// <summary>
        /// backend库
        /// </summary>
        public static string SigoBackEndConstr
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["SqlBackEndConnStr"].ConnectionString;
            }
        }

        /// <summary>
        /// B2C库
        /// </summary>
        public static string SigoB2cConstr
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["SqlB2CConnStr"].ConnectionString;
            }
        }

        /// <summary>
        /// Sigocn100库
        /// </summary>
        public static string Sigocn100Constr
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["SqlSigocn100ConnStr"].ConnectionString;
            }
        }
    }
}
