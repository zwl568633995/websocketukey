﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebSocketSharp;
using System.Text;
using System.Threading.Tasks;
using Sigo.B2C.Open.HaiTao.BLL.BusinessEntity;
using Newtonsoft.Json;
using System.Data;
using System.Data.SqlClient;
using Sigo.B2C.Open.HaiTao.BLL.Service;

namespace Sigo.B2C.Open.HaiTao.BLL.Utils
{
    /// <summary>
    /// 该类通过Socket SAR算法进行加密 by Zheng Weilin
    /// </summary>
    public class WebSocketHelper
    {              
        /// <summary>
        /// socket
        /// </summary>
        private WebSocket webSocket = null;

        /// <summary>
        /// 订单实体数据
        /// </summary>
        private RealTimeData concurentRealTimeData = null;

        /// <summary>
        /// socket
        /// </summary>
        public WebSocket WebSocket
        {
            get
            {
                return webSocket;
            }

            set
            {
                webSocket = value;
            }
        }

        /// <summary>
        /// 订单实体数据
        /// </summary>
        public RealTimeData ConcurentRealTimeData
        {
            get
            {
                return concurentRealTimeData;
            }

            set
            {
                concurentRealTimeData = value;
            }
        }

        /// <summary>
        /// socketURL
        /// </summary>
        private readonly string socketUrl = "wss://wss.singlewindow.cn:61231";  // wss://wss.singlewindow.cn:61231

        /// <summary>
        /// 海关测试URL
        /// </summary>
        private const string TESTURL = "https://swapptest.singlewindow.cn/ceb2grab/grab/realTimeDataUpload";

        /// <summary>
        /// 海关线上URL
        /// </summary>
        private const string REALURL = "https://customs.chinaport.gov.cn/ceb2grab/grab/realTimeDataUpload";

        /// <summary>
        /// 消息
        /// </summary>
        private string outMessage = string.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="WebSocketHelper"/> class.
        /// 初始化Socket
        /// </summary>
        public WebSocketHelper()
        {
            #region socket初始化订阅事件
            WebSocket = new WebSocket(socketUrl);

            WebSocket.OnOpen += (sender, e) =>
            {
            };

            WebSocket.OnMessage += (sender, e) =>
            {
                var message = "OnMessage : " + e.Data;

                ////加密Model
                SpcSignDataAsPemModel pem = null;
                try
                {
                    pem = JsonConvert.DeserializeObject<SpcSignDataAsPemModel>(e.Data.Replace("_", ""));
                }
                catch
                {
                    return;
                }

                if (ConcurentRealTimeData == null)
                {
                    return;
                }

                ////1.加密数据
                var signValue = pem.Args.Data.ToList()[0];
                ConcurentRealTimeData.SignValue = signValue;
                var payExInfo = new PayExInfo();
                payExInfo.PayExInfoStr = JsonConvert.SerializeObject(ConcurentRealTimeData, Formatting.None, JsonHelper.JsonSerializerSettings);
                var sourceStr = Utils.HttpHelper.BuildFormPostData(payExInfo, "payExInfoStr", true, ignoreNullValue: true, sortName: false);

                ////2.上传单据给海关返回结果
                var result = Utils.HttpHelper.Post(TESTURL, sourceStr, "application/x-www-form-urlencoded;utf-8");
                outMessage = result.Item2;

                ////3.海关返回的结果
                HaiGuanResponseBEntity response = JsonConvert.DeserializeObject<HaiGuanResponseBEntity>(outMessage);

                #region 日志记录我们发送单据给海关的调用结果【持久化到数据库】
                using (SqlConnection conn = new BaseService().OpenSigoB2CConnection())
                {
                    string sExcute = string.Format(
                        @"UPDATE HaiGuan_Content SET IsSuccess={0},HaiGuanResponse='{1}' WHERE SessionID='{2}'
                                                ", response.Code == "10000" ? 1 : 0,
                        JsonConvert.SerializeObject(response),
                        ConcurentRealTimeData.SessionID);

                    using (SqlCommand cmd = new SqlCommand(sExcute, conn))
                    {
                        int rows = cmd.ExecuteNonQuery();
                    }
                }
                #endregion
            };

            WebSocket.OnClose += (sender, e) =>
            {
            };

            WebSocket.OnError += (sender, e) =>
            {
            };

            WebSocket.Connect();
            #endregion
        }
    }
}
