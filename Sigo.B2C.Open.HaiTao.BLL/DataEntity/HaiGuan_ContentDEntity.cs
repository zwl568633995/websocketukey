﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sigo.B2C.Open.HaiTao.BLL.DataEntity
{
    /// <summary>
    /// 返回结果数据
    /// </summary>
    public class HaiGuan_ContentDEntity
    {
        /// <summary>
        /// 自增主键
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// 海关调用sigo接口的原始请求
        /// </summary>
        public string OrignRequest { get; set; }

        /// <summary>
        /// 海关调用sigo接口，sigo返回的结果
        /// </summary>
        public string SigoResponse { get; set; }

        /// <summary>
        /// 推送单据海关返回的数据
        /// </summary>
        public string HaiGuanResponse { get; set; }

        /// <summary>
        /// 请求SessionID
        /// </summary>
        public string SessionID { get; set; }

        /// <summary>
        /// 新增人
        /// </summary>
        public int Adder { get; set; }

        /// <summary>
        /// 新增时间
        /// </summary>
        public DateTime AddTime { get; set; }

        /// <summary>
        /// 修改人
        /// </summary>
        public int Moder { get; set; }

        /// <summary>
        /// 修改人
        /// </summary>
        public DateTime ModTime { get; set; }

        /// <summary>
        /// 是否成功
        /// </summary>
        public int IsSuccess { get; set; }
    }
}
